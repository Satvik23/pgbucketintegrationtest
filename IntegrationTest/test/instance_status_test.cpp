#include "..//include/utils.h"
#include "..//include/constants.h"
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>

using namespace Constants;

BOOST_AUTO_TEST_CASE(error_count_validation_with_valid_request) {
	int initial_error_count = get_count(error_count);

	string request = prepare_create_job_request(job_id, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_error_count == get_count(error_count));
}

BOOST_AUTO_TEST_CASE(error_count_validation_with_invalid_request) {
	int initial_error_count = get_count(error_count);
	string request = prepare_create_job_request(job_id + 1, job_name, true, frequency, kind_of_job_db, type, invalid_select_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 1, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_error_count != get_count(error_count));
}

BOOST_AUTO_TEST_CASE(success_count_validation_with_valid_request) {
	int initial_success_count = get_count(success_count);
	string request = prepare_create_job_request(job_id + 2, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 2, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_success_count != get_count(success_count));
}

BOOST_AUTO_TEST_CASE(success_count_validation_with_invalid_request) {
	int initial_success_count = get_count(success_count);
	string request = prepare_create_job_request(job_id + 3, job_name, true, frequency, kind_of_job_db, type, invalid_select_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 3, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_success_count == get_count(success_count));
}

BOOST_AUTO_TEST_CASE(fail_count_validation_with_invalid_fail_result_match) {
	int initial_fail_count = get_count(fail_count);
	string request = prepare_create_job_request(job_id + 4, job_name, true, frequency, kind_of_job_db, type, select_user_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 4, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_fail_count == get_count(fail_count));
}

BOOST_AUTO_TEST_CASE(fail_count_validation_with_valid_fail_result_match) {
	int initial_fail_count = get_count(fail_count);
	int i[] = {1, 2, 3, 4};
	string request = prepare_create_job_request_with_optional_properties(job_id + 5, job_name, true, frequency, kind_of_job_db, type, select_user_query,
		uri, true, int_array_to_string(i, 4), int_array_to_string(i, 4), true, fail_result_match_user, auto_disable_count, complete_flow_result, false, 10);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 5, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_fail_count != get_count(fail_count));
}

BOOST_AUTO_TEST_CASE(running_count_validation_without_delay) {
	int initial_running_count = get_count(running_count);
	string request = prepare_create_job_request(job_id + 6, job_name, true, frequency, kind_of_job_db, type, select_user_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(3));
	BOOST_REQUIRE(delete_job(job_id + 6, delete_job_api).result() == http::status::ok);
	BOOST_CHECK(initial_running_count == get_count(running_count));
}

BOOST_AUTO_TEST_CASE(running_count_validation_with_delay) {
	int initial_running_count = get_count(running_count);
	string request = prepare_create_job_request(job_id + 7, job_name, true, frequency, kind_of_job_db, type, sleep_query,uri);
	BOOST_REQUIRE(create_job(request).result() == http::status::ok);
	boost::this_thread::sleep(boost::posix_time::seconds(7));
	BOOST_CHECK(initial_running_count != get_count(running_count));
	BOOST_REQUIRE(delete_job(job_id + 7, delete_job_api).result() == http::status::ok);
}

BOOST_AUTO_TEST_CASE(server_uptime_validation) {
	BOOST_CHECK(get_server_uptime_seconds_value() > -1);
}