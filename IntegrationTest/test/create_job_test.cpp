#define BOOST_TEST_MODULE runseven_integration_test_cases
#include "..//include/utils.h"
#include "..//include/constants.h"
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread/thread.hpp>

using namespace Constants;

void isJobCreatationSuccess(http::status result) {
	BOOST_REQUIRE(result == http::status::ok);
}

void sleepCurrentThread(int seconds) {
	boost::this_thread::sleep(boost::posix_time::seconds(seconds));
}

void isJobDeletionSuccess(int job_id) {
	BOOST_CHECK(delete_job(job_id, delete_job_api).result() == http::status::ok);
}

void checkErrorMessage(string message, http::response<http::string_body> response) {
	BOOST_CHECK(boost::algorithm::contains(get_response_error_data(response), message));
}

void isBadRequest(http::status result) {
	BOOST_CHECK(result == http::status::bad_request);
}

BOOST_AUTO_TEST_CASE(job_id_validation_with_valid_request) {
	string request = prepare_create_job_request(job_id + 10, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 10);
}

BOOST_AUTO_TEST_CASE(job_id_validation_by_creating_job_with_exitsting_job_id) {
	string request = prepare_create_job_request(job_id + 11, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	response = create_job(request);
	BOOST_CHECK(response.result() == http::status::internal_server_error);
	checkErrorMessage(job_already_exists_msg, response);
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 11);
}

BOOST_AUTO_TEST_CASE(job_id_validation_by_creating_job_with_job_id_zero) {
	string request = prepare_create_job_request(0, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_id_msg, response);
}

BOOST_AUTO_TEST_CASE(job_id_validation_by_creating_job_with_invalid_job_id) {
	string request = prepare_create_job_request(12345678901234567889, job_name, true,  frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_id_msg, response);
}

BOOST_AUTO_TEST_CASE(job_id_validation_by_creating_job_without_job_id) {
	string request = prepare_create_job_request_without_job_id(job_name, frequency, kind_of_job_db, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_id_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_empty_job_name) {
	string request = prepare_create_job_request(job_id + 12, "", true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_name_msg, response);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_without_job_name) {
	string request = prepare_create_job_request_without_job_name(job_id + 13, frequency, kind_of_job_db, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_name_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_only_digits) {
	string request = prepare_create_job_request(job_id + 13, job_name_contains_only_digits, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 13);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_only_alphabetes) {
	string request = prepare_create_job_request(job_id + 14, job_name_contains_only_alphabetes, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 14);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_only_special_characters) {
	string request = prepare_create_job_request(job_id + 15, job_name_contains_only_special_characters, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 15);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_alphabetes_and_digits) {
	string request = prepare_create_job_request(job_id + 16, job_name_contains_digits_and_alphabets, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 16);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_alphabetes_and_special_characters) {
	string request = prepare_create_job_request(job_id + 17, job_name_contains_alphabetes_and_special_characters,true,  frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 17);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_digits_and_special_characters) {
	string request = prepare_create_job_request(job_id + 18, job_name_contains_digits_and_special_characters, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 18);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_job_with_job_name_contains_digits_and_special_characters_and_alphabetes) {
	string request = prepare_create_job_request(job_id + 19, job_name_contains_digits_and_alphabets_and_special_characters, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 19);
}

BOOST_AUTO_TEST_CASE(job_name_validation_by_creating_two_jobs_with_the_same_job_name) {
	string request = prepare_create_job_request(job_id + 20, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	request = prepare_create_job_request(job_id + 21, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 20);
	isJobDeletionSuccess(job_id + 21);
}

BOOST_AUTO_TEST_CASE(enable_property_validation_with_false) {
	string request = prepare_create_job_request(job_id + 21, job_name, false, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 21);
}

BOOST_AUTO_TEST_CASE(enable_property_validation_without_enable_property) {
	string request = prepare_create_job_request_without_enable_property(job_id + 22, job_name, frequency, kind_of_job_db, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_enable_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(enable_property_validation_with_false_and_execution) {
	int initial_success_count = get_count(success_count);
	string request = prepare_create_job_request(job_id + 23, job_name, false, frequency, kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	BOOST_CHECK(initial_success_count == get_count(success_count));
	isJobDeletionSuccess(job_id + 23);	
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_without_frequence_property) {
	string request = prepare_create_job_request_without_frequency(job_id + 23, job_name, true, kind_of_job_db, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_invalid_string_frequency_value) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "Day:day Month:*  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:month  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:hour Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:minute Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:* Second:second", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);	
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_invalid_special_character_frequency_value) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "Day:# Month:*  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:%  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:@ Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:$ Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:* Second:&", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_invalid__frequency_value) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "Month:*  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:* ", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_empty__frequency_value) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_invalid__frequency_value_format) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "Day.* Month.*  Hour.* Minute.* Second.*", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(frequency_property_validation_with_invalid__frequency_value_format2) {
	string request = prepare_create_job_request(job_id + 24, job_name, true, "Date:* Month:*  Hour:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Mon:*  Hour.* Minute.* Second.*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  our:* Minute:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minu:* Second:*", kind_of_job_db, type, valid_select_query,uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);

	request = prepare_create_job_request(job_id + 24, job_name, true, "Day:* Month:*  Hour:* Minute:* Sec:*", kind_of_job_db, type, valid_select_query,
		uri);
	response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(job_run_frequency_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(run_property_validation_without_run_property) {
	string request = prepare_create_job_request_without_run_property(job_id + 23, job_name, true, frequency, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(run_property_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(kind_property_validation_without_kind_property) {
	string request = prepare_create_job_request_without_kind_property(job_id + 10, job_name, true, frequency, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(kind_property_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(kind_property_validation_with_invalid_kind_property) {
	string request = prepare_create_job_request(job_id + 10, job_name, true, frequency, "test", type, valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(kind_property_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(type_property_validation_without_type_property) {
	string request = prepare_create_job_request_without_type_property(job_id + 10, job_name, true, frequency, kind_of_job_db, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(type_property_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(type_property_validation_with_invalid_type_property) {
	string request = prepare_create_job_request(job_id + 10, job_name, true, frequency, kind_of_job_db, "test", valid_select_query,
		uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(type_property_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(type_property_validation_with_type_as_event) {
	string request = prepare_create_job_request(job_id + 25, job_name, true, frequency, kind_of_job_db, "event", valid_select_query,uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	//Deleting this event is pending.
}

BOOST_AUTO_TEST_CASE(properties_validation_without_properties) {
	string request = prepare_create_job_request_without_properties(job_id + 25, job_name, true, frequency, kind_of_job_db, "event", fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(properties_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(query_validation_without_query_property) {
	string request = prepare_create_job_request_without_query(job_id + 10, job_name, true, frequency, kind_of_job_db, type, uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(query_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(query_validation_with_empty_query_property) {
	string request = prepare_create_job_request(job_id + 10, job_name, true, frequency, kind_of_job_db, type, "",uri);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(query_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(query_validation_with_invalid_query_property) {
	string request = prepare_create_job_request(job_id + 30, job_name, true, frequency, kind_of_job_db, type, "ssss",uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 30);
}

BOOST_AUTO_TEST_CASE(query_validation_with_query_contains_only_digits) {
	string request = prepare_create_job_request(job_id + 31, job_name, true, frequency, kind_of_job_db, type, "1234",uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 31);
}

BOOST_AUTO_TEST_CASE(query_validation_with_query_contains_only_alphabetes) {
	string request = prepare_create_job_request(job_id + 32, job_name, true, frequency, kind_of_job_db, type, "%#$@@",uri);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 32);
}

BOOST_AUTO_TEST_CASE(connection_uri_validation_without_connection_uri_property) {
	string request = prepare_create_job_request_without_connection_uri(job_id + 32, job_name, true, frequency, kind_of_job_db, type, valid_select_query,
		fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(uri_missing_msg, response);
}

BOOST_AUTO_TEST_CASE(connection_uri_validation_with_empty_connection_uri_property) {
	string request = prepare_create_job_request(job_id + 32, job_name, true, frequency, kind_of_job_db, type, valid_select_query,"");
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(uri_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(connection_uri_validation_with_invalid_connection_uri_property) {
	string request = prepare_create_job_request(job_id + 33, job_name, true, frequency, kind_of_job_db, type, valid_select_query,"ssss");
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 33);
}

BOOST_AUTO_TEST_CASE(connection_uri_validation_with_connection_uri_contains_only_digits) {
	string request = prepare_create_job_request(job_id + 34, job_name, true, frequency, kind_of_job_db, type, valid_select_query,"1234");
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 34);
}

BOOST_AUTO_TEST_CASE(connection_uri_validation_with_connection_uri_contains_only_special_characters) {
	string request = prepare_create_job_request(job_id + 35, job_name, true, frequency, kind_of_job_db, type, valid_select_query,"%&&&");
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 35);
}

BOOST_AUTO_TEST_CASE(create_job_with_invalid_property) {
	string request = prepare_create_job_request_with_invalid_property(job_id + 35, job_name, true, frequency, kind_of_job_db, type, valid_select_query,
		uri, fail_result_match, auto_disable_count, complete_flow_result);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(property_invalid_msg, response);
}

BOOST_AUTO_TEST_CASE(create_job_request_with_optional_properties) {
	int j[] = {20};
	string request = prepare_create_job_request_with_optional_properties(job_id + 36, job_name, true, frequency, kind_of_job_db, type, valid_select_query,uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 36);
}

BOOST_AUTO_TEST_CASE(store_column_validation_with_true) {
	int j[] = { 30 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 37, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		true, int_array_to_string(j, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 37);
}

BOOST_AUTO_TEST_CASE(store_column_validation_with_false) {
	int j[] = { 40 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 38);
}

BOOST_AUTO_TEST_CASE(pass_event_id_validation_with_negative_value) {
	int i[] = {-1};
	int j[] = { 13};
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_pass_event_msg, response);
}

BOOST_AUTO_TEST_CASE(fail_event_id_validation_with_negative_value) {
	int i[] = { 12};
	int j[] = { -1 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_fail_event_msg, response);
}

BOOST_AUTO_TEST_CASE(fail_event_id_validation_with_bigger_value) {
	int i[] = { 12  };
	int j[] = { 123456789123456789 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_fail_event_msg, response);
}

BOOST_AUTO_TEST_CASE(pass_event_id_validation_with_bigger_value) {
	int i[] = { 123456789123456789 };
	int j[] = { 12 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_job_pass_event_msg, response);
}

BOOST_AUTO_TEST_CASE(pass_event_id_validation_without_value) {
	int i[1] = {};
	int j[] = { 12 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 38, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		true, int_array_to_string(i, 0), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 38);
}

BOOST_AUTO_TEST_CASE(fail_event_id_validation_without_value) {
	int i[] = { 11};
	int j[1] = {};
	string request = prepare_create_job_request_with_optional_properties(job_id + 39, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		true, int_array_to_string(i, 1), int_array_to_string(j, 0), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 39);
}

BOOST_AUTO_TEST_CASE(generate_run_validation_with_false) {
	int j[] = { 40 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 40, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 40);
}

BOOST_AUTO_TEST_CASE(generate_run_validation_with_true) {
	int j[] = { 40 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 41, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), true, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 41);
}

BOOST_AUTO_TEST_CASE(auto_disable_count_validation_with_negative_value) {
	int i[] = { 1 };
	int j[] = { 12 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 42, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, -1, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_disable_fail_count_msg, response);
}

BOOST_AUTO_TEST_CASE(lock_time_validation_with_negative_value) {
	int i[] = { 1 };
	int j[] = { 12 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 42, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(i, 1), int_array_to_string(j, 1), false, fail_result_match, auto_disable_count, complete_flow_result, false, -1);
	http::response<http::string_body> response = create_job(request);
	isBadRequest(response.result());
	checkErrorMessage(invalid_timout_msg, response);
}

BOOST_AUTO_TEST_CASE(skip_next_run_validation_with_true) {
	int j[] = { 40 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 43, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), true, fail_result_match, auto_disable_count, complete_flow_result, true, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 43);
}

BOOST_AUTO_TEST_CASE(skip_next_run_validation_with_false) {
	int j[] = { 40 };
	string request = prepare_create_job_request_with_optional_properties(job_id + 44, job_name, true, frequency, kind_of_job_db, type, valid_select_query, uri,
		false, int_array_to_string(j, 1), int_array_to_string(j, 1), true, fail_result_match, auto_disable_count, complete_flow_result, false, 10);
	http::response<http::string_body> response = create_job(request);
	isJobCreatationSuccess(response.result());
	sleepCurrentThread(3);
	isJobDeletionSuccess(job_id + 44);
}