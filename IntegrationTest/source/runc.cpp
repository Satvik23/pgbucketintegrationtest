//
// <<License Header>>
//
#include "../include/utils.h"

// Globals
size_t              g_timeout{5};
size_t              g_column_count{2};
bool                g_extended_mode{false};
bool                g_orderby_latest{false};
bool                g_fetch_all_rows{false};
bool                g_print_pretty{false};
size_t              g_job_id{};
size_t              g_row_limit{10};
string              g_host_ip{"127.0.0.1"};
size_t              g_port{7171};
string              g_api_version{"1"};
string              g_hash_table{"hash"};
string              g_request_body{};
std::vector<string> g_params{};



