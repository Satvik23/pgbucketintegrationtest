//
// <<License Header>>
//
#include "../include/utils.h"
#include "..//include/constants.h"
#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace http;
using namespace Constants;

typedef map<string, string> map_type;
map_type g_map;

void u_set_request_headers(request<string_body> *request) {
	request->insert(field::content_type, "application/json");
	request->insert(field::from, VERSION);
	request->insert(field::timeout, TO_STR(5));
}


string int_array_to_string(int int_array[], int size_of_array) {
	ostringstream oss("");
	for (int temp = 0; temp < size_of_array; temp++)
		oss << int_array[temp];
	return oss.str();
}

response<string_body> u_contact_server_get_response(const request<string_body> &request ) {
	// The io_context is required for all I/O
	boost::asio::io_context ioc;

	// These objects perform our I/O
	tcp::resolver resolver{ioc};
	tcp::socket   socket{ioc};

	// Look up the domain name
	auto const results = resolver.resolve(host_ip, TO_STR( port ) );

	// Make the connection on the IP address we get from a lookup
	boost::asio::connect( socket, results.begin(), results.end() );

	// Send the HTTP request to the remote host
	write( socket, request );

	// This buffer is used for reading and must be persisted
	boost::beast::flat_buffer buffer;

	// Declare a container to hold the response
	response<string_body> result;

	// Receive the HTTP response
	read( socket, buffer, result );

	return result;
}

string get_server_instance_status() {
	string data = "";
	stringstream target_str;
	target_str << "/v" << "1" << "/instance-status";

	request<string_body> request{ verb::get, target_str.str(), 11 };
	u_set_request_headers(&request);
	request.prepare_payload();
	response<string_body> response = u_contact_server_get_response(request);
	if (response.result() == status::ok) {
		return get_response_message_data(response);
	}
	return data;
}

string get_response_message_data(response<string_body> response) {
	string data = "";
	const json response_data = json::parse(response.body());
	if (response_data.size() != 0 && response_data["error"].get<string>().empty()
		&& !response_data["message"].get<string>().empty()) {
		return response_data["message"].get<string>();
	}
	return data;
}

string get_response_error_data(response<string_body> response) {
	string data = "";
	const json response_data = json::parse(response.body());
	if (response_data.size() != 0 && !response_data["error"].get<string>().empty()) {
		return response_data["error"].get<string>();
	}
	return data;
}

string prepare_create_job_request(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}}}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true);
}

string prepare_create_job_request_without_job_id(string job_name, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		%name %double_quote%job_name%double_quote  %enable % true % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_job_name(int job_id, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %enable % true % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_enable_property(int job_id, string job_name, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_frequency(int job_id, string job_name, bool enable_value, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_run_property(int job_id, string job_name, bool enable_value, string frequency, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s,  %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote  % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_kind_property(int job_id, string job_name, bool enable_value, string frequency,  string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_type_property(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_properties(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string fail_result_match, int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		 % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_query(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_without_connection_uri(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_with_invalid_property(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% invalid_id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str % true % generate_run % false
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % false % lock_timeout % 10);
}

string prepare_create_job_request_with_optional_properties(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri, bool store_columns, string pass_event_ids, string fail_event_ids, bool generate_run_flag, string fail_result_match,
	int auto_disable_count, string complete_flow_result, bool skip_next_run_flag, int lock_timeout_value) {
	return str(boost::format("{ %s:%d, %s:%s%s%s, %s:%b, %s:%s%s%s, %s : { %s:%s%s%s, %s:%s%s%s, %s : {%s:%s%s%s, %s:%s%s%s,%s:%b}} , %s:[%s], %s:[%s], %s:%b, %s:%s%s%s, %s:%d, %s:%s%s%s, %s:%b, %s:%d}")
		% id %job_id %name %double_quote%job_name%double_quote  %enable %enable_value % frequency_str %double_quote% frequency%double_quote %run %kind %double_quote%kind_of_job%double_quote  %type_str %double_quote%type%double_quote
		%properties %query_str %double_quote%query%double_quote %connection_uri %double_quote%uri%double_quote %store_columns_str %store_columns %pass_event_id_str %pass_event_ids %fail_event_id_str %fail_event_ids %generate_run % generate_run_flag
		% fail_result_match_str %double_quote%fail_result_match%double_quote %auto_disable_count_str %auto_disable_count
		%complete_flow_result_str %double_quote%complete_flow_result%double_quote %skip_next_run % skip_next_run_flag % lock_timeout % lock_timeout_value);
}


response<string_body> create_job(string request_data) {
	stringstream target_str;
	target_str << "/v" << "1" << "/create";
	request<string_body> request{ verb::post, target_str.str(), 11 };
	json                             body = json::parse(request_data);
	request.body() = body.dump();
	u_set_request_headers(&request);
	request.prepare_payload();
	return u_contact_server_get_response(request);
}

response<string_body> request_delete_job(string api) {
	request<string_body> request{ verb::delete_, api, 11 };
	u_set_request_headers(&request);
	request.prepare_payload();
	return u_contact_server_get_response(request);
}

response<string_body>  delete_job_with_string_job_id(string job_id, string api) {
	stringstream target_str;
	target_str << "/v" << "1" << api << job_id;
	return request_delete_job(target_str.str());
}

// delete job
response<string_body>  delete_job(int job_id, string api) {
	stringstream target_str;
	target_str << "/v" << "1" << api << job_id;
	return request_delete_job(target_str.str());
}



int get_server_uptime_seconds_value() {
	json  response_data;
	vector<string> strs;
	string server_uptime;
	int server_uptime_seconds_value = -1;
	response_data["data"] = json::parse(get_server_instance_status());
	for (auto &x : response_data["data"].items()) {
		g_map[x.key()] = x.value().dump();
	}
	auto uptime_itr = g_map.find("uptime");
	if (uptime_itr != g_map.end()) {
		server_uptime = uptime_itr->second;
		boost::erase_all(server_uptime, "\"");
		boost::split(strs, server_uptime, boost::is_any_of(":"));
	}
	for (size_t i = 0; i < strs.size(); i++) {
		if (i == 2) {
			server_uptime_seconds_value = boost::lexical_cast<int>(strs[i]);
		}
	}
	return server_uptime_seconds_value;
}

int get_count(string count_name ) {
	json  response_data;
	int count = -1;
	response_data["data"] = json::parse(get_server_instance_status());
	for (auto &x : response_data["data"].items()) {
		g_map[x.key()] = x.value().dump();
	}
	auto count_itr = g_map.find(count_name);
	if (count_itr != g_map.end())
		count = boost::lexical_cast<int>(count_itr->second);
	return count;
}