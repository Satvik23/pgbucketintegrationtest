//
// <<License Header>>
//
#ifndef INCLUDE_UTILS_H_
#define INCLUDE_UTILS_H_
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/status.hpp>
#include <boost/beast/version.hpp>
#include <boost/date_time/local_time/local_date_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/common.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/program_options.hpp>
#include <nlohmann/json.hpp>
#include <string>

#define VERSION "runc 2.0-dev"
#define TO_STR( X ) std::to_string( X )

namespace posixTime = boost::posix_time;

namespace popts = boost::program_options;
namespace src = boost::log::sources;
namespace http = boost::beast::http;

using json = nlohmann::json;
using string = std::string;
using tcp = boost::asio::ip::tcp;

http::response<http::string_body> u_contact_server_get_response(const http::request<http::string_body> &request );
void        u_set_request_headers( http::request<http::string_body> *request );
string get_server_instance_status();
http::response<http::string_body> create_job(const string request_data);
http::response<http::string_body>  delete_job(int job_id, string delete_job_api);
int get_server_uptime_seconds_value();
int get_count(std::string count_name);
string prepare_create_job_request_with_optional_properties(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri, bool store_columns, string pass_event_ids, string fail_event_ids, bool generate_run, string fail_result_match,
	int auto_disable_count, string complete_flow_result, bool skip_next_run, int lock_timeout);
string get_response_message_data(http::response<http::string_body> response);
string get_response_error_data(http::response<http::string_body> response);
string prepare_create_job_request_without_job_id(string job_name, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_job_name(int job_id, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_enable_property(int job_id, string job_name, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_frequency(int job_id, string job_name, bool enable_value, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_run_property(int job_id, string job_name, bool enable_value, string frequency, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_kind_property(int job_id, string job_name, bool enable_value, string frequency, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_type_property(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_properties(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string fail_result_match, int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_query(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_without_connection_uri(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request_with_invalid_property(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri, string fail_result_match,
	int auto_disable_count, string complete_flow_result);
string prepare_create_job_request(int job_id, string job_name, bool enable_value, string frequency, string kind_of_job, string type,
	string query, string uri);
string int_array_to_string(int int_array[], int size_of_array);
http::response<http::string_body> request_delete_job(string api);
http::response<http::string_body>  delete_job_with_string_job_id(string job_id, string api);
#endif /* INCLUDE_UTILS_H_ */
