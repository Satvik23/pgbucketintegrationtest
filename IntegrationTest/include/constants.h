#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string.h>

using namespace std;

namespace Constants
{
	const string host_ip("127.0.0.1");
	const int port (8080);
	const int  job_id(600);
	const string job_name("test job");
	const string frequency("Day:* Month:*  Hour:* Minute:* Second:*");
	const string kind_of_job_db("db");
	const string type("job");
	const string valid_select_query("select now()");
	const string invalid_select_query("select now(");
	const string uri("postgres://localhost:5432/postgres");
	const string fail_result_match("text");
	const string fail_result_match_user("Sharma");
	const int auto_disable_count(3);
	const string complete_flow_result("some result");
	const string select_user_query("select user");
	const string error_count("error-count");
	const string success_count("success-count");
	const string fail_count("fail-count");
	const string running_count("running-count");
	const string sleep_query("select pg_sleep(10)");
	const string id("\"id\"");
	const string name("\"name\"");
	const string enable("\"enable\"");
	const string frequency_str("\"frequency\"");
	const string run("\"run\"");
	const string kind("\"kind\"");
	const string type_str("\"type\"");
	const string properties("\"properties\"");
	const string query_str("\"query\"");
	const string connection_uri("\"connection_uri\"");
	const string store_columns_str("\"store_columns\"");
	const string pass_event_id_str("\"pass_event_id\"");
	const string fail_event_id_str("\"fail_event_id\"");
	const string generate_run("\"generate_run\"");
	const string fail_result_match_str("\"fail_result_match\"");
	const string auto_disable_count_str("\"auto_disable_count\"");
	const string complete_flow_result_str("\"complete_flow_result\"");
	const string skip_next_run("\"skip_next_run\"");
	const string lock_timeout("\"lock_timeout\"");
	const string double_quote("\"");
	const string invalid_job_pass_event_msg("invalid job pass events");
	const string invalid_job_fail_event_msg("invalid job fail event values");
	const string invalid_disable_fail_count_msg("Invalid disable fail count value");
	const string invalid_timout_msg("Invalid timeout value");
	const string job_already_exists_msg("Job is already exists");
	const string invalid_job_id_msg("Job id is invalid");
	const string invalid_job_name_msg("Job name is invalid ");
	const string job_id_missing_msg("Job id is missing");
	const string job_name_missing_msg("Job name is missing");
	const string job_enable_missing_msg("Job enable is missing");
	const string job_run_frequency_missing_msg("Job run frequency is missing");
	const string job_run_frequency_invalid_msg("job run frequency is invalid");
	const string run_property_missing_msg("missing run property");
	const string kind_property_missing_msg("job kind is missing");
	const string type_property_missing_msg("job type is missing");
	const string type_property_invalid_msg("invalid job type");
	const string kind_property_invalid_msg("Invalid job kind value");
	const string properties_missing_msg("missing job properties");
	const string job_id__missing_msg("Job id is missing");
	const string query_missing_msg("missing job query");
	const string query_invalid_msg("query is invalid");
	const string uri_missing_msg("missing job connection uri");
	const string uri_invalid_msg("invalid connection uri");
	const string property_invalid_msg("job property is invalid");
	const string job_is_not_found("Job is not found");
	const string invalid_target_msg("invalid target");
	const string job_name_contains_only_digits("628790");
	const string job_name_contains_only_alphabetes("JOB");
	const string job_name_contains_only_special_characters("!@#$%^&*(");
	const string job_name_contains_digits_and_alphabets("123SSS");
	const string job_name_contains_digits_and_special_characters("123#$");
	const string job_name_contains_alphabetes_and_special_characters("SSSS###??");
	const string job_name_contains_digits_and_alphabets_and_special_characters("123SSS####$$$");
	const string invalid_id("\"id2\"");
	const string delete_job_api("/delete?id=");
}

#endif

